from math import factorial, log2, sqrt
from typing import NamedTuple


# Terminology

class CodeParams(NamedTuple):
    n: int
    k: int
    w: int


def list_dec_w(n: int, t: int) -> int:
    return int(n - sqrt(n * (n - 2 * t - 2)))


def list_dec_t(n: int, w: int) -> int:
    t = w
    while list_dec_w(n, t) >= w:
        t -= 1
    return t + 1


def w_to_t(w: int, n: int, list_dec: bool=False) -> int:
    return list_dec_t(n, w) if list_dec else w


class GoppaParams(NamedTuple):
    n: int
    m: int
    t: int   
    
    def code_params(self, list_dec: bool=False) -> CodeParams:
        n, m, t = self
        return CodeParams(n,
                          n - m * t,
                          list_dec_w(n, t) if list_dec else t)


class PlotkinParams(NamedTuple):
    g1: GoppaParams
    g2: GoppaParams
    
    def code_params(self, list_dec: bool=False) -> CodeParams:
        c1 = self.g1.code_params(list_dec)
        c2 = self.g2.code_params(list_dec)
        return CodeParams(c1.n + max(c1.n, c2.n),
                          c1.k + c2.k,
                          min(c2.w, 2 * c1.w))


# Combinatorics

comb_save = [[1] for i in range(4096 + 1)]


def comb(n: int, k: int) -> float:
    if n < 0 or n < k or k < 0:
        raise ValueError()
    last = len(comb_save[k])
    if n - k < last:
        return comb_save[k][n - k]
    result = comb_save[k][-1]
    for i in range(last, n - k + 1):
        result *= i + k
        result //= i
        comb_save[k].append(result)
    return result


def lcomb(n: int, k: int) -> float:
    return log2(comb(n, k))


# Key size


def pub_key_size(p: CodeParams, indcca2: bool=False) -> int:
    return p.k * (p.n - p.k) if indcca2 else p.k * p.n


# Security parameters

def _sec_bcd_bound_p(pa: CodeParams, p: int) -> float:
    return lcomb(pa.n, pa.w) \
           - lcomb(pa.n - pa.k, pa.w - p) \
           - lcomb(pa.k, p) / 2 \
           - 1


def sec_bcd_bound(pa: CodeParams) -> float:  
    try:
        mi = None
        for p in range(0, pa.w):
            ne = _sec_bcd_bound_p(pa, p)
            if mi is None or ne < mi:
                mi = ne
            else:
                break
        return mi or 0
    except:
        return 0


# Brute-force Search


def find_best_goppa_config(target_f, restrict_to_pow2, fixed_t=None, min_n=None):
    best_target = None, None
    best_config = None
    for m in range(2, 13):
        for n in range(min(1e9 if min_n is None else min_n, 2**m if restrict_to_pow2 else 2**(m-1)), 2**m + 1, 10):
            for t in range(1, n//(2*m) + 1) if fixed_t is None else [fixed_t]:
                config = GoppaParams(n, m, t)
                target = target_f(config)
                if target[1] is None:
                    continue
                if best_target[1] is None or target[1] < best_target[1]:
                    best_target = target
                    best_config = config
                    if min_n is None:
                        print(best_config, best_target)
            
    return (best_config, best_target[0]), best_target[1]



def mceliece_best_config(sec_level_f,
                         target_sec_level,
                         list_dec=False,
                         indcca2=False,
                         restrict_to_pow2=False):
    def check_and_get(config):
        params = config.code_params(list_dec)
        if sec_level_f(params) < target_sec_level:
            return None, None
        return None, pub_key_size(params, indcca2)
    return find_best_goppa_config(check_and_get,
                                  restrict_to_pow2)



def plotkin_best_config_inner(sec_level_f,
                              target_sec_level,
                              outer_config,
                              list_dec=False,
                              indcca2=False,
                              restrict_to_pow2=False):
    def check_and_get(config):
        params = PlotkinParams(outer_config, config).code_params(list_dec)
        if sec_level_f(params) < target_sec_level:
            return None, None
        return None, pub_key_size(params, indcca2)
    return find_best_goppa_config(check_and_get,
                                  restrict_to_pow2,
                                  #fixed_t=outer_config[2]*2,
                                  min_n=outer_config[0])



def plotkin_best_config(sec_level_f,
                        target_sec_level,
                        list_dec=False,
                        indcca2=False,
                        restrict_to_pow2=False):
    return find_best_goppa_config(lambda c: plotkin_best_config_inner(sec_level_f,
                                                                      target_sec_level,
                                                                      c,
                                                                      list_dec,
                                                                      indcca2,
                                                                      restrict_to_pow2),
                                  restrict_to_pow2)


# Fast Search


def best_config_fast(sec_level_f,
                             target_sec_level,
                             max_k_f,
                             list_dec=False,
                             indcca2=False,
                             restrict_to_pow2=False):
    assert indcca2
    best_target = None
    best_config = None
    for n in range(1000, 10000):
        for w in range(1, int(n / (2 * log2(n))) + 10):
            k = max_k_f(n, w, list_dec)
            pa = CodeParams(n, k, w)
            if sec_level_f(pa) >= target_sec_level:
                target = pub_key_size(pa, indcca2)
                if best_target is None or target < best_target:
                    best_target = target
                    best_config = pa
                    #print(pa, target)
    return best_config, best_target


# Plotkin


m1m2_save = dict()
def possible_m1m2(n):
    if n in m1m2_save:
        return m1m2_save[n]
    m1m2_save[n] = []
    for m1 in range(1, 14):
        for m2 in range(m1, 14):
            if 2**m1 + 2**m2 >= n:
                if len(m1m2_save[n]) == 0 or m1m2_save[n][-1][1] > m2:
                    m1m2_save[n].append((m1, m2))
                break
    return m1m2_save[n]


def min_m(t: int) -> int:
    for m in range(1, 1000):
        if 2**m >= 2 * m * t:
            return m
    raise Exception()


def plotkin_max_k(n, w, list_dec):
    w2_lower = w_to_t(w, n, list_dec)
    w1_lower = w_to_t((w + 1) // 2, n, list_dec)
    k = 0
    for m1, m2 in possible_m1m2(n):
        m1 = max(m1, min_m(w1_lower))
        m2 = max(m2, min_m(w2_lower))
        k = max(k, n - w1_lower * m1 - w2_lower * m2)
    return k


def plotkin_reverse(pa: CodeParams, list_dec: bool=False) -> PlotkinParams:
    n, k, w = pa
    
    w2 = w_to_t(w, n, list_dec)
    w1 = w_to_t((w + 1) // 2, n, list_dec)
    for m1, m2 in possible_m1m2(n):
        m1 = max(m1, min_m(w1))
        m2 = max(m2, min_m(w2))
        if n - w1* m1 - w2 * m2 == pa.k:
            n1 = min(2 ** m1, n//2)
            result = PlotkinParams(GoppaParams(n1, m1, w1), GoppaParams(n - n1, m2, w2))
            assert result.code_params(list_dec) == pa
            return result
    raise Exception()


def plotkin_best_config_fast(sec_level_f,
                             target_sec_level,
                             list_dec=False,
                             indcca2=False,
                             restrict_to_pow2=False):
    params, size = best_config_fast(sec_level_f,
                              target_sec_level,
                              plotkin_max_k,
                              list_dec,
                              indcca2,
                              restrict_to_pow2)
    return params, plotkin_reverse(params, list_dec), size


# McEliece


def m_from_n(n: int) -> int:
    return int(log2(n - 1)) + 1


def mceliece_max_k(n: int, w: int, list_dec: bool=False) -> int:
    return n - m_from_n(n) * w_to_t(w, n, list_dec)


def mceliece_reverse(pa: CodeParams, list_dec: bool=False) -> GoppaParams:
    result = GoppaParams(pa.n, m_from_n(pa.n), w_to_t(pa.w, pa.n, list_dec))
    assert result.code_params(list_dec) == pa
    return result


def mceliece_best_config_fast(sec_level_f,
                              target_sec_level,
                              list_dec=False,
                              indcca2=False,
                              restrict_to_pow2=False):
    params, size = best_config_fast(sec_level_f,
                              target_sec_level,
                              mceliece_max_k,
                              list_dec,
                              indcca2,
                              restrict_to_pow2)
    return params, mceliece_reverse(params, list_dec), size


# Aggregation


results = []


def stats(*args, **kwargs):
    global results
    results = []
    for list_dec in [True, False]:
        results.append([])
        for sec_level in [80, 128, 192, 256]:
            results[-1].append([])
            results[-1][-1].append(sec_level)
            kwargs['target_sec_level'] = sec_level
            kwargs['sec_level_f'] = sec_bcd_bound
            kwargs['list_dec'] = list_dec
            kwargs['indcca2'] = True
            mceliece_code_params, mceliece_config, mceliece_size = mceliece_best_config_fast(*args, **kwargs)
            results[-1][-1].append((mceliece_config, mceliece_size))
            plotkin_code_params, plotkin_config, plotkin_size = plotkin_best_config_fast(*args, **kwargs)    
            results[-1][-1].append((plotkin_config, plotkin_size))
            print(results[-1][-1])

results = \
[[
[80, (GoppaParams(n=1875, m=11, t=40), 631400), (PlotkinParams(g1=GoppaParams(n=512, m=9, t=20), g2=GoppaParams(n=2010, m=11, t=41)), 1193221)],
[128, (GoppaParams(n=3026, m=12, t=76), 1927968), (PlotkinParams(g1=GoppaParams(n=2042, m=11, t=40), g2=GoppaParams(n=2042, m=11, t=81)), 3664243)],
[192, (GoppaParams(n=5168, m=13, t=100), 5028400), (PlotkinParams(g1=GoppaParams(n=3661, m=12, t=46), g2=GoppaParams(n=3661, m=12, t=93)), 9430872)],
[256, (GoppaParams(n=7107, m=13, t=127), 9007856), (PlotkinParams(g1=GoppaParams(n=2048, m=11, t=69), g2=GoppaParams(n=7473, m=13, t=138)), 17789304)],
],
[
[80, (GoppaParams(n=1893, m=11, t=42), 661122), (PlotkinParams(g1=GoppaParams(n=512, m=9, t=22), g2=GoppaParams(n=2035, m=11, t=44)), 1271930)],
[128, (GoppaParams(n=3307, m=12, t=66), 1991880), (PlotkinParams(g1=GoppaParams(n=1024, m=10, t=32), g2=GoppaParams(n=3611, m=12, t=64)), 3859136)],
[192, (GoppaParams(n=5397, m=13, t=97), 5215496), (PlotkinParams(g1=GoppaParams(n=1024, m=10, t=47), g2=GoppaParams(n=6396, m=13, t=94)), 9691776)],
[256, (GoppaParams(n=7150, m=13, t=131), 9276241), (PlotkinParams(g1=GoppaParams(n=2048, m=11, t=65), g2=GoppaParams(n=7915, m=13, t=130)), 18176990)],
]]


def pretty_print(bits):
    bys = bits / 8
    suffix = ['', 'Ki', 'Mi']
    i = 0
    while bys > 512:
        bys /= 1024
        i += 1
    return '{:.2f} {}B'.format(bys, suffix[i])


def print_table(list_dec=True):
    global results
    cr = results[0 if list_dec else 1]
    sizes = [(a[1][1], a[2][1]) for a in cr]
    sizes1 = [pretty_print(a[0]) for a in sizes]
    sizes2 = [pretty_print(a[1]) for a in sizes]
    diff = ['x{:.2f}'.format(a[1] / a[0]) for a in sizes]
    table = fr'''
\begin{{table}}
 \small
 \renewcommand{{\arraystretch}}{{1.5}}
 \centering
 \begin{{tabularx}}{{\textwidth}}{{|@{{ }}c|*{{4}}{{X|}}}}
    \hline
    \multirow{{2}}{{*}}{{Система}} &
    \multicolumn{{4}}{{c|}}{{Уровень безопасности}} \\
    \cline{{2-5}}
    & 80 bit & 128 bit & 192 bit & 256 bit \\
    \hline
    \hline
    McEliece & {' & '.join(sizes1)} \\
    \hline
    Плоткин & {' & '.join(sizes2)} \\
    \hline
    \hline
    Прирост & {' & '.join(diff)} \\
    \hline
  \end{{tabularx}}
  \caption{{параметры {'с' if list_dec else 'без'} List Decoding}}
\end{{table}}
'''
    print(table)
